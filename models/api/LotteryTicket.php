<?php

namespace app\models\api;

use app\components\ApiClient;

class LotteryTicket {

    public $id;
    public $userId;
    public $ticket;
    public $saleId;
    public $date;

    /**
     * @param $userId
     * @return array
     */
    public static function get($userId)
    {
        $apiClient = new ApiClient('lottery/getTickets/' . $userId);

        $response = $apiClient->get();

        return self::_getResults($response);
    }

    /**
     * Convert response from API
     *
     * @param $data
     * @return array
     */
    private static function _getResults($data)
    {
        $result = [];

        if ($data) {
            if (! is_array($data)) {
                $data = [$data];
            }
            foreach ($data as $object) {
                $news = new self;

                $news->id   = $object->_id;
                $news->userId = $object->userId;
                $news->saleId = $object->saleId;
                $news->ticket = $object->ticket;
                $news->date = strtotime($object->date);

                $result[] = $news;
            }
        }

        return $result;
    }

}