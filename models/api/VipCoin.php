<?php

namespace app\models\api;

use app\components\ApiClient;

class VipCoin {

    /**
     * @param $data
     * @return mixed
     */
    public static function createCertificate($data)
    {
        $apiClient = new ApiClient('certificate/vipcoin');

        return $apiClient->post($data);
    }
}