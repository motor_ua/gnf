<?php

namespace app\modules\business\models;

use yii\base\Model;
use app\components\THelper;


class SharesCertificateForm extends Model {

    public $id;
    public $full_name;
    public $country;
    public $city;
    public $address;
    public $phone;
    public $skype;
    public $messenger;
    public $messenger_number;

    /**
     * @return array
     */
    public function rules() {
        return [
            [['full_name', 'country', 'city', 'address', 'phone', 'messenger', 'messenger_number'], 'required', 'message' => THelper::t('required_field')],
            [['skype'], 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            'full_name' => THelper::t('full_name'),
            'skype' => THelper::t('skype'),
        ];
    }
}
