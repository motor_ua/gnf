<?php

namespace app\modules\business\models;

use yii\base\Model;
use app\components\THelper;


class PdfCertificateForm extends Model {

    public $id;
    public $full_name;

    /**
     * @return array
     */
    public function rules() {
        return [
            [['full_name'], 'required', 'message' => THelper::t('required_field')],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            'full_name' => THelper::t('full_name'),
        ];
    }
}
