<?php
    use app\components\THelper;
?>
<div class="panel panel-success">
    <div class="panel-heading"><?= THelper::t('success') ?></div>
    <div class="panel-body"><?=!empty($message) ? $message : THelper::t('success')?></div>
</div>