<?php
use app\components\THelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>


<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title"><?= THelper::t('shares_certificate') ?></h4>
        </div>
        <div class="modal-body">
            <div class="row" style="margin-bottom: 25px">
                <div class="col-xs-12">


                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?= $form->field($model, 'full_name', ['enableAjaxValidation' => false])->textInput()->label(THelper::t('full_name')) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?= $form->field($model, 'country', ['enableAjaxValidation' => false])->dropDownList($countries, ['prompt' => THelper::t('select_country')])->label(THelper::t('select_country')); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?= $form->field($model, 'city')->textInput()->label(THelper::t('city')) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?= $form->field($model, 'address')->textInput()->label(THelper::t('address')) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?= $form->field($model, 'phone')->textInput()->label(THelper::t('phone')) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?= $form->field($model, 'skype')->textInput()->label(THelper::t('skype')) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?= $form->field($model, 'messenger', ['enableAjaxValidation' => false])->dropDownList([
                                'whatsapp' => THelper::t('whatsapp'),
                                'viber'    => THelper::t('viber'),
                                'telegram' => THelper::t('telegram'),
                                'facebook' => THelper::t('facebook'),
                            ], ['id' => 'messenger', 'prompt' => THelper::t('select_messenger')])->label(THelper::t('messenger_info')) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?= $form->field($model, 'messenger_number')->textInput()->label(THelper::t('messenger_number')) ?>
                        </div>
                    </div>

                    <div class="text-center">
                        <?= Html::submitButton(THelper::t('send'), ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                    <p>
                        <?= THelper::t('vip_coin_certificate_info')?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
