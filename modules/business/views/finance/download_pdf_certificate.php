<?php
use app\components\THelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>


<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title"><?= THelper::t('download_pdf_certificate') ?></h4>
        </div>
        <div class="modal-body">
            <div class="row" style="margin-bottom: 25px">
                <div class="col-xs-12">

                    <?php $form = ActiveForm::begin(); ?>

                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?= $form->field($model, 'full_name', ['enableAjaxValidation' => false])->textInput()->label(THelper::t('full_name')) ?>
                        </div>
                    </div>

                    <div class="text-center">
                        <?= Html::submitButton(THelper::t('submit'), ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
