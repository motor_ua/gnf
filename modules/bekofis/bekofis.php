<?php

namespace app\modules\bekofis;

class bekofis extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\bekofis\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
