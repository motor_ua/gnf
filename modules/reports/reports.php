<?php

namespace app\modules\reports;

class reports extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\reports\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
